#!/usr/bin/env python
# coding: utf-8

# # US Birth Rate Analysis
# #By- Aarush Kumar
# #Dated: Sept. 21,2021

# In[1]:


#import library
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics


# ## Data Extraction

# In[2]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/US Birth Rate Analysis/Data/US_births_2000-2014_SSA.csv')


# In[3]:


df


# In[4]:


df.shape


# In[5]:


df.size


# In[6]:


df.info()


# In[7]:


df.isnull().sum()


# In[8]:


df.dtypes


# In[9]:


df.describe()


# In[10]:


df.describe().T


# In[11]:


plt.figure(figsize = (10,6))
sns.heatmap(df.astype(float).corr(), linewidths = 0.25, vmax = 1.0, square = True, annot = True)
plt.title("Pearson Correlation")


# ## Linear Regression Model

# In[12]:


#split data
X = df.drop('births', axis = 1)
y = df['births']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 1)
print(X_train.shape)
print(y_train.shape)
print(X_test.shape)
print(y_test.shape)


# In[13]:


linreg = LinearRegression()
linreg.fit(X_train, y_train)
get_ipython().run_line_magic('time', 'linreg.fit(X_train, y_train)')


# In[14]:


#prediction
y_pred = linreg.predict(X_test)
print(y_pred)


# In[15]:


#result
result = pd.DataFrame({'Actual' : y_test, 'Prediction' : y_pred})
result.head()


# In[16]:


#check MAE
print('Mean Absolute Error : ', metrics.mean_absolute_error(y_test, y_pred).round(2))


# In[17]:


#check MSE
print('Mean Squared Error : ', metrics.mean_squared_error(y_test, y_pred).round(2))


# In[18]:


#check RMSE
print('Root Mean Squared Error : ', np.sqrt(metrics.mean_absolute_error(y_test, y_pred).round(2)))


# In[19]:


#check r2 score
print('r2_score : ', metrics.r2_score(y_test, y_pred))


# In[20]:


#visualization model
x = y_test
y = y_pred

plt.figure(figsize = (10,6))
plt.plot(x, y, 'o')
plt.title("Linear Regression Model")

m, b = np.polyfit(x, y, 1)
plt.plot(x, m * x + b)


# In[21]:


#distribution
plt.figure(figsize = (10,6))
sns.distplot(df['births'])
plt.title("Distribution of Births")
plt.show()


# In[22]:


#trend birth by year
plt.figure(figsize = (10,6))
sns.lineplot(data = df, x = 'year', y = 'births')
plt.title("Births ~ Year")
plt.show()

